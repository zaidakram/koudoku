module Koudoku
  class ApplicationController < ::ApplicationController
    unless ::ApplicationController.superclass == ActionController::API
      layout Koudoku.layout
      helper :application
    end

  end
end
